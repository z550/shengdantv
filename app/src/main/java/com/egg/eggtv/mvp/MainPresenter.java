package com.egg.eggtv.mvp;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.egg.eggtv.net.ErrorBean;
import com.egg.eggtv.net.ReqCallBack;
import com.egg.eggtv.net.RequestManager;
import com.egg.eggtv.pojo.BannerEntity;
import com.egg.eggtv.pojo.DefaultVideoEntity;
import com.egg.eggtv.pojo.ShopEntity;
import com.egg.eggtv.pojo.ShopInfoEntity;
import com.egg.eggtv.pojo.VideoEntity;
import com.egg.eggtv.util.API;

import java.util.HashMap;
import java.util.List;

/**
 * Created by JoeLjt on 2019/9/18.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class MainPresenter {

    private Context mContext;
    private IMainView mView;

    private DownloadManager mDownloadManager;

    public MainPresenter(Context mContext, IMainView mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    /**
     * 获取门店列表
     */
    public void requestShopList(String companyId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", companyId);
        RequestManager.getInstance().requestGetByAsyn(API.BASE_URL + API.GET_SHOP_LIST, hashMap, new ReqCallBack<List<ShopEntity>>() {
            @Override
            public void onReqSuccess(List<ShopEntity> result) {
                mView.initTvDialog(result);
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * 根据 key 值获取当前设备绑定的门店信息
     *
     * @param deviceId
     */
    public void getShopInfoByDeviceId(String deviceId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("code", deviceId);
        RequestManager.getInstance().requestGetByAsyn(API.BASE_URL + API.GET_SHOP_ID, hashMap, new ReqCallBack<ShopInfoEntity>() {
            @Override
            public void onReqSuccess(ShopInfoEntity result) {
                mView.showTvView(result.getShopId());
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * 绑定门店
     *
     * @param
     */
    public void bindDeviceWithShop(final ShopEntity shopEntity, final String deviceId) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", API.COMPANY_ID);
        hashMap.put("shopId", shopEntity.getName());
        hashMap.put("deviceId", deviceId);
        hashMap.put("deviceType", "EQ06");

        RequestManager.getInstance().requestPostByAsyn(API.BASE_URL + API.POST_BIND_DEVICE, hashMap, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(String result) {
                // 绑定成功后，将数据存储到
                mView.saveDeviceId(deviceId, shopEntity.getValue());
                mView.showTvView(shopEntity.getName());
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getBannerPic() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", API.COMPANY_ID);
        hashMap.put("position", "0");

        RequestManager.getInstance().requestGetByAsyn(API.BASE_URL + API.GET_BANNER_LIST, hashMap, new ReqCallBack<List<BannerEntity>>() {
            @Override
            public void onReqSuccess(List<BannerEntity> result) {
                mView.loadUpBannerPicture(result);
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getPlayVideo() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", API.COMPANY_ID);
        hashMap.put("position", "2");
        hashMap.put("status", "1");
        hashMap.put("appId", "2");
        hashMap.put("pageNum", "1");
        hashMap.put("pageSize", "20");

        RequestManager.getInstance().requestGetByAsyn(API.BASE_URL + API.GET_VIDEO_URL, hashMap, new ReqCallBack<VideoEntity>() {
            @Override
            public void onReqSuccess(VideoEntity result) {
                mView.playVideo(result);
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getDefaultVideoUrl() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", API.COMPANY_ID);
        hashMap.put("position", "2");
        hashMap.put("appId", "2");

        RequestManager.getInstance().requestGetByAsyn(API.BASE_URL + API.GET_DEFAULT_VIDEO_URL,
                hashMap, new ReqCallBack<List<DefaultVideoEntity>>() {
            @Override
            public void onReqSuccess(List<DefaultVideoEntity> resultList) {
                mView.playDefaultVideo(getDefaultVideoUrlFromList(resultList));
            }

            @Override
            public void onFailure(String result) {
                Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReqFailed(ErrorBean error) {
                Toast.makeText(mContext, error.getMsg(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private String getDefaultVideoUrlFromList(List<DefaultVideoEntity> resultList) {
        if (resultList != null) {
            for (DefaultVideoEntity videoEntity : resultList) {
                int position = videoEntity.getPosition();
                if (position == 2) {
                    return videoEntity.getDefaultImg();
                }
            }
        }
        return null;
    }

}
