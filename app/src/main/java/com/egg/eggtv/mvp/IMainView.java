package com.egg.eggtv.mvp;

import com.egg.eggtv.pojo.BannerEntity;
import com.egg.eggtv.pojo.ShopEntity;
import com.egg.eggtv.pojo.VideoEntity;

import java.util.List;

/**
 * Created by JoeLjt on 2019/9/18.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public interface IMainView {

    void initTvDialog(List<ShopEntity> result);

    void saveDeviceId(String deviceId, String shopName);

    void playVideo(VideoEntity imgUrl);

    void loadUpBannerPicture(List<BannerEntity> bannerList);

    void showTvView(String shopId);

    void playDefaultVideo(String defaultUrl);
}
