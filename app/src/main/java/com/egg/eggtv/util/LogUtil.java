package com.egg.eggtv.util;

import android.util.Log;

import com.egg.eggtv.BuildConfig;

/**
 * auther: qiuzhenhuan
 * date: 2018/10/27 下午6:03
 * desc: log 工具类
 */
public class LogUtil {

    private static boolean isDebug = BuildConfig.DEBUG;
    private static final String TAG = "com.egg.eggtv";


    public static void e(String tag, String msg) {
        if (isDebug) {
            Log.e(tag, msg);
        }
    }

    public static void e(String msg) {
        e(TAG, msg);
    }


    public static void d(String tag, String msg) {
        if (isDebug) {
            Log.d(TAG, msg);
        }
    }

    public static void d(String msg) {
        d(TAG, msg);
    }

    public static void i(String tag, String msg) {
        if (isDebug) {
            Log.i(TAG, msg);
        }
    }

    public static void i(String msg) {
        i(TAG, msg);
    }

    public static void w(String tag, String msg) {
        if (isDebug) {
            Log.i(TAG, msg);
        }
    }

    public static void w(String msg) {
        w(TAG, msg);
    }


}
