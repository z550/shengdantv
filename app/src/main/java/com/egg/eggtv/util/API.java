package com.egg.eggtv.util;

/**
 * Created by JoeLjt on 2019/8/9.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class API {

    public static String DEBUG_COMPANY_ID = "79d41327588547c39647b2701dcbc857";
    public static String RELEASE_COMPANY_ID = "E74ECFF824000000A000000000160000";

    public static String DEBUG_URL = "http://test.mumzone.cn";
    public static String RE_URL = "http://re.mumzone.cn";
    public static String RELEASE_URL = "http://service.mumzone.cn";

    public static String COMPANY_ID = "";

    public static String BASE_URL = "";

    public static String GET_SHOP_ID = "/egg_basic/api/v1.6/app/equipment/getEquipmentByEqCode";

    public static String GET_SHOP_LIST = "/egg_basic/api/v1.6/app/shop/shopList";

    public static String POST_BIND_DEVICE = "/egg_basic/api/v1.6/app/equipment/addEquipment";

    public static String GET_VIDEO_URL = "/egg_member/api/v1.6/adv";

    public static String GET_DEFAULT_VIDEO_URL = "/egg_member/api/v1.6/adv/overview";

    public static String GET_BANNER_LIST = "/egg_member/api/v1.6/app/banner/bannerList";

}
