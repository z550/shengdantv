package com.egg.eggtv.ui.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.egg.eggtv.R;

import java.util.Map;
import java.util.Set;

/**
 * Created by JoeLjt on 2019/6/14.
 * Email: lijiateng1219@gmail.com
 * Description: 统一封装的弹窗基类
 */

public class TvDialog extends AlertDialog {

    private static int mConfirmType;

    public static final int CONFIRM_TYPE_NORMAL = -200;
    public static final int CONFIRM_TYPE_SINGLE_CHOICE_SHADOW = -300;
    public static final int CONFIRM_TYPE_DOUBLE_CHOICE_SHADOW = -400;
    public static final int CONFIRM_TYPE_WITH_TEXT_SHADOW = -500;

    public TvDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    public void setContentView(@NonNull View view) {
        super.setContentView(view);
    }

    public static class PosBuilder extends AlertDialog.Builder {

        private Context mContext;

        public PosBuilder(Context context) {
            super(context);
            this.mContext = context;

            View layout = LayoutInflater.from(mContext).inflate(R.layout.dialog_tv, null);
            super.setContentView(layout);
            super.setWidthAndHeight(438, 312);
       }

    }

}
