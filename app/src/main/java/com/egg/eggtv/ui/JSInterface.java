package com.egg.eggtv.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.egg.eggtv.App;

/**
 * Created by JoeLjt on 2019/9/26.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class JSInterface {

    public static final int VIDEO_DURATION_MSG = 911;

    private Context context;
    private Handler handler;

    public JSInterface(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    @JavascriptInterface
    public void getVideoDuration(int duration) {
        Log.e("ljt", "getVideoDuration => " + duration);
        if (duration > 0) {
            loopTheVideo(duration);
        }
    }

    private void loopTheVideo(int duration) {
        Message msg = Message.obtain();
        msg.what = VIDEO_DURATION_MSG;
        msg.obj = duration;
//        handler.sendMessageDelayed(msg, (long) ((duration - 0.5) * 1000));
        handler.sendMessage(msg);
    }

}
