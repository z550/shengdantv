package com.egg.eggtv.ui.widgets;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.egg.eggtv.pojo.BannerEntity;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by JoeLjt on 2019/10/30.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        BannerEntity bannerEntity = (BannerEntity) path;
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(context).load(bannerEntity.getImgUrl()).into(imageView);
    }
}