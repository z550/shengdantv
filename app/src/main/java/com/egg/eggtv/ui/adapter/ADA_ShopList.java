package com.egg.eggtv.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.egg.eggtv.R;
import com.egg.eggtv.pojo.ShopEntity;

import java.util.List;

/**
 * Created by JoeLjt on 2019/9/18.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class ADA_ShopList extends RecyclerView.Adapter<ADA_ShopList.ShopViewHolder> {

    private Context mContext;
    private List<ShopEntity> mData;

    private LayoutInflater mInflater;

    private int mFocusedPosition = 0;

    public List<ShopEntity> getData() {
        return mData;
    }

    public ADA_ShopList(Context context, List<ShopEntity> data) {
        this.mContext = context;
        this.mData = data;

        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = mInflater.inflate(R.layout.item_shop_list, viewGroup, false);
        return new ShopViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopViewHolder holder, final int i) {
        final ShopEntity shopEntity = mData.get(i);
        // value 是 shopName，name 是 shopId
        holder.mTvShopName.setText(shopEntity.getValue());

        holder.rootView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
//                    holder.mTvShopName.setTextColor(Color.WHITE);
                    if (listener != null) {
                        listener.onItemFocusChanged(holder.rootView, mFocusedPosition);
                    }
                } else {
//                    holder.mTvShopName.setTextColor(Color.BLACK);
                }
            }
        });

        holder.rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        shopListGoUp();
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                        shopListGoDown();
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                        if (listener != null) {
                            listener.onCenterButtonClicked(shopEntity);
                        }
                    } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (listener != null) {
                            listener.onBackButtonClicked();
                        }
                    }
                }

                return true;
            }
        });

        if (i == mFocusedPosition) {
            holder.rootView.requestFocus();
            holder.mTvShopName.setTextColor(Color.WHITE);
        } else {
            holder.rootView.clearFocus();
            holder.mTvShopName.setTextColor(Color.BLACK);
        }

    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    private void shopListGoUp() {
        if (mFocusedPosition == 0) {
            return;
        }
        mFocusedPosition--;
        notifyDataSetChanged();
    }

    private void shopListGoDown() {
        if (mFocusedPosition == mData.size() - 1) {
            return;
        }
        mFocusedPosition += 1;
        notifyDataSetChanged();
    }

    public String getSelectedShopId() {
        if (mFocusedPosition < 0 || mFocusedPosition > mData.size() - 1) {
            throw new RuntimeException("FocusPosition out of range.");
        }
        return mData.get(mFocusedPosition).getValue();
    }

    class ShopViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout rootView;
        private TextView mTvShopName;

        ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.ll_root);
            mTvShopName = itemView.findViewById(R.id.tv_shop_name);
        }
    }

    public interface OnListener {
        void onItemFocusChanged(View rootView, int position);

        void onBackButtonClicked();

        void onCenterButtonClicked(ShopEntity shopEntity);
    }

    private OnListener listener;

    public void setListener(OnListener listener) {
        this.listener = listener;
    }
}
