package com.egg.eggtv.ui.widgets;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.egg.eggtv.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By Dlx
 * on 2018/12/19
 */
public class RingContent extends View {

    private int innerRingColor;
    private float bracketsWidth;
    private RectF colorRect;
    //控件宽，整体
    private int mHeight;
    //控件高，整体
    private int mWidth;
    //两个环形间的间距
    private float ringsMargin = 50;
    private float outRingMarginLeft;
    private float getOutRingMarginRight;
    // 外层环形的margin属性
    private float outRingMargin = 5;
    //xml中填写的颜色集合，根据颜色数量平分270°环饼图，支持1-9个颜色
    private List<Integer> colorList = new ArrayList<>();
    //区间范围，这里是终点集合，起点默认为0，有几个部分要填写几个端点
    private List<Float> nodeList = new ArrayList<>();
    private List<String> stringList = new ArrayList<>();
    //内部Padding常量，用以完全绘制四角
    private final int PADDING = 10;
    //下部容器 bound 渐变颜色，起点颜色和终点颜色
    private int boundBackgroundStartColor, boundBackgroundEndColor;
    private int cornerStrokeColor;
    private float cornerStrokeWidth;
    private float cornerLineWidth;
    private float cornerLineHeight;
    private float outRingWidth;
    private float innerRingWidth;
    private float ringCenterTextSize;
    //中心字体颜色
    private int ringCenterTextColor;
    //中心字 内容
    private float ringCenterText;

    private String ringUnderText = "";
    private float ringUnderTextSize;
    private int ringUnderTextColor;

    private int ringPartTextColor;
    private float ringPartTextSize;
    private float ringPartTextMarginTop;
    private float ringPartTextMarginBottom;
    private float ringPartTextMarginRight;
    private float ringPartTextMarginLeft;
    private float ringPartTextLineSpace;
    private RectF bracketsRect;
    private float contentSquareWidth;
    private int bracketsColor;
    boolean isTxt = false;

    public RingContent(Context context) {
        super(context);
    }

    public RingContent(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RingContent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RingChartView);
        ringsMargin = array.getDimension(R.styleable.RingChartView_two_ring_margin, 10);
        outRingMargin = array.getDimension(R.styleable.RingChartView_out_ring_margin, 0);
        maxData = array.getFloat(R.styleable.RingChartView_ring_maxData, 100);
        setWillNotDraw(false);
        int color = array.getColor(R.styleable.RingChartView_ring_part_1st_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_2nd_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_3rd_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_4th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_5th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_6th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_7th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_8th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }
        color = array.getColor(R.styleable.RingChartView_ring_part_9th_color, -1);
        if (color != -1) {
            colorList.add(color);
        }

        // value
        float node = array.getFloat(R.styleable.RingChartView_ring_node_1st_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_2nd_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_3rd_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_4th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_5th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_6th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_7th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_8th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        node = array.getFloat(R.styleable.RingChartView_ring_node_9th_value, -1);
        if (node != -1) {
            nodeList.add(node);
        }
        String str = array.getString(R.styleable.RingChartView_ring_part_1st_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_2nd_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_3rd_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_4th_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_5th_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_6th_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_7th_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_8th_text);
        if (str != null) {
            stringList.add(str);
        }
        str = array.getString(R.styleable.RingChartView_ring_part_9th_text);
        if (str != null) {
            stringList.add(str);
        }
        ringPartTextColor = array.getColor(R.styleable.RingChartView_ring_part_text_color, Color.BLACK);
        ringPartTextSize = array.getDimension(R.styleable.RingChartView_ring_part_text_size, 15);
        ringPartTextMarginTop = array.getDimension(R.styleable.RingChartView_ring_part_text_margintop, 15);
        ringPartTextMarginLeft = array.getDimension(R.styleable.RingChartView_ring_part_text_marginleft, 15);
        ringPartTextMarginRight = array.getDimension(R.styleable.RingChartView_ring_part_text_marginright, 15);
        ringPartTextMarginBottom = array.getDimension(R.styleable.RingChartView_ring_part_text_marginbottom, 15);
        ringPartTextLineSpace = array.getDimension(R.styleable.RingChartView_ring_part_text_linespace, 20);

        boundBackgroundStartColor = array.getColor(R.styleable.RingChartView_bound_background_color_start, Color.argb(0, 0, 0, 0));
        boundBackgroundEndColor = array.getColor(R.styleable.RingChartView_bound_background_color_end, Color.argb(128, 0, 0, 128));

        cornerStrokeWidth = array.getDimension(R.styleable.RingChartView_bound_corner_stroke_width, 0);
        cornerStrokeColor = array.getColor(R.styleable.RingChartView_bound_corner_stroke_color, Color.WHITE);
        cornerLineWidth = array.getDimension(R.styleable.RingChartView_bound_corner_line_width, 0);
        cornerLineHeight = array.getDimension(R.styleable.RingChartView_bound_corner_line_height, 0);
        outRingWidth = array.getDimension(R.styleable.RingChartView_out_ring_stroke_width, 30);
        innerRingWidth = array.getDimension(R.styleable.RingChartView_inner_ring_stroke_width, 5);
        innerRingColor = array.getColor(R.styleable.RingChartView_inner_ring_stroke_color, Color.parseColor("#32B8F3"));
        ringCenterTextSize = array.getDimension(R.styleable.RingChartView_ring_center_text_size, 20);
        ringCenterTextColor = array.getColor(R.styleable.RingChartView_ring_center_text_color, Color.BLACK);
        ringCenterText = array.getFloat(R.styleable.RingChartView_ring_center_text, 0);

        ringUnderText = array.getString(R.styleable.RingChartView_ring_under_title_text);
        ringUnderTextColor = array.getColor(R.styleable.RingChartView_ring_under_title_text_color, Color.BLACK);
        ringUnderTextSize = array.getDimension(R.styleable.RingChartView_ring_under_title_text_size, 20);

        contentSquareWidth = array.getDimension(R.styleable.RingChartView_ring_content_square_width, 10);
        colorRect = new RectF(0, 0, contentSquareWidth, contentSquareWidth);

        //仪表盘 外 括号粗细
        bracketsWidth = array.getDimension(R.styleable.RingChartView_out_brackets_stroke_width, 0);
        bracketsColor = array.getColor(R.styleable.RingChartView_out_brackets_stroke_color, Color.parseColor("#32B8F3"));
        currentData = ringCenterText;
    }


    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    int diameter;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mHeight = getHeight() - PADDING;
        mWidth = getWidth() - PADDING;
        diameter = mHeight > mWidth ? mWidth : mHeight;
//        getOutRing(diameter);
    }



    private void drawPointer(Canvas canvas) {
        mPaint.setStrokeCap(Paint.Cap.BUTT); //设置笔触 方形
        mPaint.setStyle(Paint.Style.STROKE);
        for (int i = 0, count = colorList.size(); i < count; i++) {
            mPaint.setColor(colorList.get(i));
            mPaint.setStrokeWidth(outRingWidth);
            drawColorRectAndText(canvas, colorList.get(i), i);
        }
    }

    public void setCurrentData(float currentData) {
        if (currentData > nodeList.get(nodeList.size() - 1)) {
            Toast.makeText(getContext(), "数据越界，请检查后再试", Toast.LENGTH_SHORT).show();
            return;
        }
        this.currentData = currentData;
        invalidate();
    }

    Rect textR = new Rect();

    private void drawColorRectAndText(Canvas canvas, int color, int position) {
        float perLineHeight = (colorRect.height() > ringPartTextSize ? colorRect.height() : ringPartTextSize) + ringPartTextLineSpace;
        canvas.save();
        if (position >= stringList.size()) {
            stringList.add("empty");
        }
        mPaint.setStrokeWidth(0);
        mPaint.setColor(ringPartTextColor);
        mPaint.setTextSize(ringPartTextSize);
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.getTextBounds(stringList.get(position), 0, stringList.get(position).length(), textR);

        canvas.translate(ringPartTextMarginLeft + PADDING,  ringPartTextMarginTop + ((textR.height() + ringPartTextLineSpace) * position));
        canvas.drawText(stringList.get(position), 10, contentSquareWidth / 2 + contentSquareWidth / 2, mPaint);
        rectPaint.setColor(color);
        rectPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(colorRect, rectPaint);
        canvas.restore();
    }

    private void drawGradientBound(Canvas canvas) {
        Shader shader = new LinearGradient(PADDING / 2, PADDING / 2, PADDING / 2, mHeight,
                boundBackgroundStartColor, boundBackgroundEndColor, Shader.TileMode.CLAMP);
        mPaint.setShader(shader);
        RectF rectF = new RectF(PADDING / 2, PADDING / 2, mWidth, mHeight);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(rectF, mPaint);
        mPaint.setShader(null);
        mPaint.setColor(cornerStrokeColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.SQUARE);
        mPaint.setStrokeWidth(cornerStrokeWidth);
        Path path = new Path();
        path.moveTo(cornerLineWidth + PADDING / 2, PADDING / 2);
        path.rLineTo(-cornerLineWidth, 0);
        path.rLineTo(0, cornerLineHeight);

        path.moveTo(cornerLineWidth + PADDING / 2, mHeight);
        path.rLineTo(-cornerLineWidth, 0);
        path.rLineTo(0, -cornerLineHeight);

        path.moveTo(mWidth - cornerLineWidth - PADDING / 2, mHeight);
        path.rLineTo(cornerLineWidth + PADDING / 2, 0);
        path.rLineTo(0, -cornerLineHeight);

        path.moveTo(mWidth - cornerLineWidth - PADDING / 2, PADDING / 2);
        path.rLineTo(cornerLineWidth + PADDING / 2, 0);
        path.rLineTo(0, cornerLineHeight);

        canvas.drawPath(path, mPaint);
    }

    public void setMaxData(float maxData) {
        this.maxData = maxData;
    }

    private float maxData;
    private float currentData = 0;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setHinting(1);
//        drawRingCenterText(canvas);
        drawGradientBound(canvas);
        drawPointer(canvas);
    }

    private void fromTempToNewAnimate(float temp, float newData) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "CurrentData", temp, newData);
        animator.setDuration(1000);
        animator.start();
    }

    public void setDataWithAnimate(float data) {
        fromTempToNewAnimate(currentData, data);
    }
}
