package com.egg.eggtv;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.egg.eggtv.util.API;


public class App extends Application {

    public static Application mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        API.COMPANY_ID = BuildConfig.DEBUG ? API.DEBUG_COMPANY_ID : API.RELEASE_COMPANY_ID; // 预发布 & 线上

        API.BASE_URL = BuildConfig.DEBUG ? API.DEBUG_URL : API.RELEASE_URL;

    }

    public static Application getInstance() {
        return mInstance;
    }


}
