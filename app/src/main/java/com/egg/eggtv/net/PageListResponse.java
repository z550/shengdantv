package com.egg.eggtv.net;

import java.util.List;

/**
 * Created by qiuzhenhuan on 2018/12/10.
 */

public class PageListResponse<T> {

    private List<T> list;

    private String pageTotal;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public String getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(String pageTotal) {
        this.pageTotal = pageTotal;
    }
}
