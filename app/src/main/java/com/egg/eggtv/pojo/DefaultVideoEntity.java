package com.egg.eggtv.pojo;

/**
 * Created by JoeLjt on 2019/10/30.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class DefaultVideoEntity {

    /**
     * defaultImg : null
     * count : 1
     * position : 0
     * used : 0
     * free : 1
     */

    private String defaultImg;
    private int count;
    private int position;
    private int used;
    private int free;

    public String getDefaultImg() {
        return defaultImg;
    }

    public void setDefaultImg(String defaultImg) {
        this.defaultImg = defaultImg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }

    public int getFree() {
        return free;
    }

    public void setFree(int free) {
        this.free = free;
    }
}
