package com.egg.eggtv.pojo;

/**
 * Created by JoeLjt on 2019/9/18.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class ShopEntity {

    private String value; // 名称
    private String name; // id

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ShopEntity{" +
                "value='" + value + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
