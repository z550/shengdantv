package com.egg.eggtv.pojo;

/**
 * Created by JoeLjt on 2019/8/9.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class ShopInfoEntity {

    private String shopId;
    private String companyId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
