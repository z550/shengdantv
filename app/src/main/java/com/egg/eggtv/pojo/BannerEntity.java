package com.egg.eggtv.pojo;

/**
 * Created by JoeLjt on 2019/10/30.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class BannerEntity {

    /**
     * bannerId : BA3A286C00000064000000000D7000
     * imgUrl : http://test.mumzone.cn/egg_public/upload/getFile?id=bcae3f8d7cf14043b07172bb7beea3eb
     * linkType : 1
     * linkUrl : dc8600756fa94a26a3c378dc784930e8
     * backgroundColor : FFB024
     * shopType : 0
     */

    private String bannerId;
    private String imgUrl;
    private int linkType;
    private String linkUrl;
    private String backgroundColor;
    private String shopType;

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getLinkType() {
        return linkType;
    }

    public void setLinkType(int linkType) {
        this.linkType = linkType;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }
}
