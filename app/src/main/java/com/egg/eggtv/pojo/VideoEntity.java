package com.egg.eggtv.pojo;

import java.util.List;

/**
 * Created by JoeLjt on 2019/10/30.
 * Email: lijiateng1219@gmail.com
 * Description:
 */

public class VideoEntity {

    /**
     * totalRecord : 1
     * list : [{"id":"A3B38325000000300000000007C400","title":"8888","position":2,"imgUrl":"http://sd-picture-oss.oss-cn-beijing.aliyuncs.com/5b80abcf28b5965b3590746e8784885d.mp4/mp4/06dedd386baa4c0e94fcb7fcf88cd84e.mp4","validityStartTime":1566466640000,"validityEndTime":null,"linkType":0,"linkUrl":"","companyId":"79d41327588547c39647b2701dcbc857","status":1,"releaseTime":1566466640000,"updateTime":1566466640000,"createTime":1566459332000,"dr":0,"appId":"2","backgroundColor":"FFB024","priorityRating":null,"otherAttributes":null}]
     */

    private int totalRecord;
    private String defaultImg;
    private List<ListBean> list;

    public String getDefaultImg() {
        return defaultImg;
    }

    public void setDefaultImg(String defaultImg) {
        this.defaultImg = defaultImg;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : A3B38325000000300000000007C400
         * title : 8888
         * position : 2
         * imgUrl : http://sd-picture-oss.oss-cn-beijing.aliyuncs.com/5b80abcf28b5965b3590746e8784885d.mp4/mp4/06dedd386baa4c0e94fcb7fcf88cd84e.mp4
         * validityStartTime : 1566466640000
         * validityEndTime : null
         * linkType : 0
         * linkUrl :
         * companyId : 79d41327588547c39647b2701dcbc857
         * status : 1
         * releaseTime : 1566466640000
         * updateTime : 1566466640000
         * createTime : 1566459332000
         * dr : 0
         * appId : 2
         * backgroundColor : FFB024
         * priorityRating : null
         * otherAttributes : null
         */

        private String id;
        private String title;
        private int position;
        private String imgUrl;
        private long validityStartTime;
        private Object validityEndTime;
        private int linkType;
        private String linkUrl;
        private String companyId;
        private int status;
        private long releaseTime;
        private long updateTime;
        private long createTime;
        private int dr;
        private String appId;
        private String backgroundColor;
        private Object priorityRating;
        private Object otherAttributes;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public long getValidityStartTime() {
            return validityStartTime;
        }

        public void setValidityStartTime(long validityStartTime) {
            this.validityStartTime = validityStartTime;
        }

        public Object getValidityEndTime() {
            return validityEndTime;
        }

        public void setValidityEndTime(Object validityEndTime) {
            this.validityEndTime = validityEndTime;
        }

        public int getLinkType() {
            return linkType;
        }

        public void setLinkType(int linkType) {
            this.linkType = linkType;
        }

        public String getLinkUrl() {
            return linkUrl;
        }

        public void setLinkUrl(String linkUrl) {
            this.linkUrl = linkUrl;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getReleaseTime() {
            return releaseTime;
        }

        public void setReleaseTime(long releaseTime) {
            this.releaseTime = releaseTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getBackgroundColor() {
            return backgroundColor;
        }

        public void setBackgroundColor(String backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public Object getPriorityRating() {
            return priorityRating;
        }

        public void setPriorityRating(Object priorityRating) {
            this.priorityRating = priorityRating;
        }

        public Object getOtherAttributes() {
            return otherAttributes;
        }

        public void setOtherAttributes(Object otherAttributes) {
            this.otherAttributes = otherAttributes;
        }
    }
}
